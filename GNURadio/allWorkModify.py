from gnuradio import blocks
import pmt
from gnuradio import digital
from gnuradio import filter
from gnuradio import fec
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import zeromq
from gnuradio.filter import pfb
import leo
import rf_scoe
import json
from socket import *

class rf_scoe_autogen(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "RF SCOE Autogen")

        ##################################################
        # Variables
        ##################################################
        self.up_center_freq = up_center_freq = 2.24e9
        self.bpsk_center_freq = bpsk_center_freq = 2.24e9
        self.pass_sim_sat_tx_ant = pass_sim_sat_tx_ant = leo.custom_antenna_make(3,bpsk_center_freq, 0, 0, 4, 360, 0)
        self.pass_sim_sat_rx_ant = pass_sim_sat_rx_ant = leo.custom_antenna_make(3,up_center_freq, 0, 0, 4, 360, 0)
        self.pass_sim_sat = pass_sim_sat = leo.satellite_make('FLP', '1 42831U 17042G   21229.01570423 -.00000289  00000-0 -23718-4 0  9996', '2 42831  97.4630  95.5502 0012795 310.4637  49.5463 14.91329456222712', bpsk_center_freq, up_center_freq, 29.8, pass_sim_sat_tx_ant, pass_sim_sat_rx_ant, 1, 210, 12000)
        self.pass_sim_gs_tx_ant = pass_sim_gs_tx_ant = leo.parabolic_reflector_antenna_make(2, up_center_freq, 0,0.5, 4.5, 50)
        self.pass_sim_gs_rx_ant = pass_sim_gs_rx_ant = leo.parabolic_reflector_antenna_make(2, bpsk_center_freq, 0,0.5, 4.5, 50)
        self.up_symb_rate = up_symb_rate = 30000
        self.up_sps = up_sps = 4
        self.up_rolloff = up_rolloff = 0.35
        self.up_nfilts = up_nfilts = 32
        self.pass_sim_gs = pass_sim_gs = leo.tracker_make(pass_sim_sat, 48.749569, 9.103410, 1, '2021-08-19T20:29:34.00Z', '2021-08-19T20:41:45.00Z', 100000, up_center_freq, bpsk_center_freq, 49, pass_sim_gs_tx_ant, pass_sim_gs_rx_ant, 1, 202, 4200000)
        self.bpsk_symb_rate = bpsk_symb_rate = 50000
        self.bpsk_sps = bpsk_sps = 8
        self.bpsk_rolloff = bpsk_rolloff = 0.35
        self.bpsk_nfilts = bpsk_nfilts = 32
        self.up_samp_rate = up_samp_rate = up_symb_rate * up_sps
        self.up_rrc_taps = up_rrc_taps = firdes.root_raised_cosine(up_nfilts, up_nfilts, 1.0/float(up_sps), up_rolloff, 45*up_nfilts)
        self.up_pass_sim_model = up_pass_sim_model = leo.leo_model_make(pass_sim_gs, 0, 5,
          													0, 0,
          													1, 3,
          													True, 7.5, 0, 25)
        self.up_conv_enc = up_conv_enc = fec.cc_encoder_make(80,7, 2, [79,-109], 0, fec.CC_STREAMING, False)
        self.up_const = up_const = digital.constellation_bpsk().base()
        self.down_pass_sim_model = down_pass_sim_model = leo.leo_model_make(pass_sim_gs, 1, 5,
          													0, 0,
          													1, 3,
          													True, 7.5, 0, 25)
        self.down_conv_dec_1_0 = down_conv_dec_1_0 = fec.cc_decoder.make(80,7, 2, [79,-109], 0, -1, fec.CC_STREAMING, False)
        self.down_conv_dec_1 = down_conv_dec_1 = fec.cc_decoder.make(80,7, 2, [79,-109], 0, -1, fec.CC_STREAMING, False)
        self.down_const = down_const = digital.constellation_rect([0.707+0.707j, -0.707+0.707j, -0.707-0.707j, 0.707-0.707j], [0, 2, 3, 1],
        4, 2, 2, 1, 1).base()
        self.bpsk_samp_rate = bpsk_samp_rate = bpsk_symb_rate * bpsk_sps
        self.bpsk_rrc_taps = bpsk_rrc_taps = firdes.root_raised_cosine(bpsk_nfilts, bpsk_nfilts, 1.0/float(bpsk_sps), bpsk_rolloff, 45*bpsk_nfilts)
        self.bpsk_conv_enc = bpsk_conv_enc = fec.cc_encoder_make(80,7, 2, [79, -109], 0, fec.CC_STREAMING, False)
        self.bpsk_conv_dec_1 = bpsk_conv_dec_1 = fec.cc_decoder.make(80,7, 2, [79, -109], 0, -1, fec.CC_STREAMING, False)
        self.bpsk_conv_dec_0 = bpsk_conv_dec_0 = fec.cc_decoder.make(80,7, 2, [79, -109], 0, -1, fec.CC_STREAMING, False)
        self.bpsk_const = bpsk_const = digital.constellation_bpsk().base()

        ##################################################
        # Blocks
        ##################################################
        self.file_source = blocks.file_source(gr.sizeof_char*1, '/home/holeczcc/gnuradio/transfer/GUI/RawCadu.raw', True, 0, 0)
        self.file_source.set_begin_tag(pmt.PMT_NIL)
        self.zeromq_sub_msg_source_0_0 = zeromq.sub_msg_source('tcp://127.0.0.1:2221', 100)
        self.zeromq_sub_msg_source_0 = zeromq.sub_msg_source('tcp://127.0.0.1:2220', 100)
        self.zeromq_pub_msg_sink_0_1 = zeromq.pub_msg_sink('tcp://127.0.0.1:4440', 1000)
        self.zeromq_pub_msg_sink_0_0_1_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:5554', 100)
        self.zeromq_pub_msg_sink_0_0_1 = zeromq.pub_msg_sink('tcp://127.0.0.1:4444', 100)
        self.zeromq_pub_msg_sink_0_0_0_2 = zeromq.pub_msg_sink('tcp://127.0.0.1:5550', 100)
        self.zeromq_pub_msg_sink_0_0_0_1_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:5551', 100)
        self.zeromq_pub_msg_sink_0_0_0_1 = zeromq.pub_msg_sink('tcp://127.0.0.1:4441', 100)
        self.zeromq_pub_msg_sink_0_0_0_0_0_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:5553', 100)
        self.zeromq_pub_msg_sink_0_0_0_0_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:4443', 100)
        self.zeromq_pub_msg_sink_0_0_0_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:3334', 100)
        self.zeromq_pub_msg_sink_0_0_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:3331', 100)
        self.zeromq_pub_msg_sink_0_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:3333', 100)
        self.zeromq_pub_msg_sink_0 = zeromq.pub_msg_sink('tcp://127.0.0.1:3330', 100)
        self.up_unpack_0 = blocks.unpack_k_bits_bb(8)
        self.up_unpack = blocks.unpack_k_bits_bb(8)
        self.up_pass_sim_channel_1 = leo.channel_model(bpsk_samp_rate, down_pass_sim_model, 0)
        self.up_pass_sim_channel = leo.channel_model(up_samp_rate, up_pass_sim_model, 0)
        self.up_pack_0 = blocks.pack_k_bits_bb(8)
        self.up_pack = blocks.pack_k_bits_bb(8)
        self.up_fec_enc_0_0 = fec.extended_encoder(encoder_obj_list=up_conv_enc, threading= None, puncpat='11')
        self.up_fec_enc = fec.extended_encoder(encoder_obj_list=bpsk_conv_enc, threading= None, puncpat='11')
        self.up_dif_enc_0_0 = digital.diff_encoder_bb(len(up_const.points()))
        self.up_dif_enc = digital.diff_encoder_bb(len(bpsk_const.points()))
        self.up_const_mod_2 = digital.chunks_to_symbols_bc(down_const.points(), )
        self.up_const_mod_0 = digital.generic_mod(
            constellation=up_const,
            differential=False,
            samples_per_symbol=up_sps,
            pre_diff_code=True,
            excess_bw=0.35,
            verbose=False,
            log=False)
        self.up_const_mod = digital.generic_mod(
            constellation=bpsk_const,
            differential=False,
            samples_per_symbol=bpsk_sps,
            pre_diff_code=True,
            excess_bw=0.35,
            verbose=False,
            log=False)
        self.sim_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, up_samp_rate,True)
        self.sim_throttle = blocks.throttle(gr.sizeof_gr_complex*1, bpsk_samp_rate,True)
        self.rf_scoe_asm_sync_bb_0 = rf_scoe.asm_sync_bb('00011010110011111111110000011101', 0, False)
        self.pfb_arb_resampler_xxx_0_0_0 = pfb.arb_resampler_ccf(
            bpsk_sps,
            taps=bpsk_rrc_taps,
            flt_size=32)
        self.pfb_arb_resampler_xxx_0_0_0.declare_sample_delay(0)
        self.file_source_0 = blocks.file_source(gr.sizeof_char*1, '/home/holeczcc/gnuradio/transfer/GUI/RawCaduCo.raw', True, 0, 0)
        self.file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.down_sym_sync_1 = digital.symbol_sync_cc(
            digital.TED_SIGNAL_TIMES_SLOPE_ML,
            bpsk_sps,
            0.0628,
            1.0,
            1.0,
            1.5,
            1,
            digital.constellation_bpsk().base(),
            digital.IR_PFB_MF,
            32,
            bpsk_rrc_taps)
        self.down_sym_sync_0 = digital.symbol_sync_cc(
            digital.TED_SIGNAL_TIMES_SLOPE_ML,
            up_sps,
            0.0628,
            1.0,
            1.0,
            1.5,
            1,
            digital.constellation_bpsk().base(),
            digital.IR_PFB_MF,
            32,
            up_rrc_taps)
        self.down_sym_sync = digital.symbol_sync_cc(
            digital.TED_SIGNAL_TIMES_SLOPE_ML,
            bpsk_sps,
            0.0628,
            1.0,
            1.0,
            1.5,
            1,
            digital.constellation_bpsk().base(),
            digital.IR_PFB_MF,
            32,
            bpsk_rrc_taps)
        self.down_stat_snr_1 = digital.probe_mpsk_snr_est_c(3, 10000, 0.001)
        self.down_stat_snr_0 = digital.probe_mpsk_snr_est_c(3, 1000, 0.001)
        self.down_stat_snr = digital.probe_mpsk_snr_est_c(3, 10000, 0.001)
        self.down_stat_frame_lock_1_0 = rf_scoe.frame_lock_b('00011010110011111111110000011101', 0, 1279)
        self.down_stat_frame_lock_0 = rf_scoe.frame_lock_b('00011010110011111111110000011101', 0, 1279)
        self.down_stat_frame_lock = rf_scoe.frame_lock_b('00011010110011111111110000011101', 0, 1279)
        self.down_fec_delay = blocks.delay(gr.sizeof_float*1, 1)
        self.down_fec_dec_1 = fec.extended_decoder(decoder_obj_list=bpsk_conv_dec_1, threading= None, ann=None, puncpat='11', integration_period=10000)
        self.down_fec_dec_0_0_0_0_1 = fec.extended_decoder(decoder_obj_list=down_conv_dec_1, threading= None, ann=None, puncpat='11', integration_period=10000)
        self.down_fec_dec_0_0 = fec.extended_decoder(decoder_obj_list=down_conv_dec_1_0, threading= None, ann=None, puncpat='11', integration_period=10000)
        self.down_fec_dec_0 = fec.extended_decoder(decoder_obj_list=bpsk_conv_dec_0, threading= None, ann=None, puncpat='11', integration_period=10000)
        self.down_dif_dec_1 = digital.diff_decoder_bb(len(bpsk_const.points()))
        self.down_dif_dec_0_1 = digital.diff_decoder_bb(len(down_const.points()))
        self.down_dif_dec_0_0_0_0_1 = digital.diff_decoder_bb(len(down_const.points()))
        self.down_dif_dec_0_0 = digital.diff_decoder_bb(len(up_const.points()))
        self.down_dif_dec_0 = digital.diff_decoder_bb(len(bpsk_const.points()))
        self.down_const_soft_dec = digital.constellation_soft_decoder_cf(bpsk_const)
        self.down_const_recv = digital.constellation_receiver_cb(down_const, 0.0628, 0, 1024)
        self.down_const_null_0 = blocks.null_sink(gr.sizeof_char*1)
        self.down_const_null = blocks.null_sink(gr.sizeof_char*1)
        self.down_asm_sync_0 = rf_scoe.asm_sync_bb('00011010110011111111110000011101', 0, False)
        self.down_asm_sync = rf_scoe.asm_sync_bb('00011010110011111111110000011101', 0, False)
        self.digital_map_bb_1_1_0 = digital.map_bb([1, 3, 0, 2])
        self.digital_map_bb_1_0_0 = digital.map_bb(down_const.pre_diff_code())
        self.digital_map_bb_0_0_0_0_1 = digital.map_bb([down_const.pre_diff_code().index(i) for i in range(len(down_const.pre_diff_code()))])
        self.digital_map_bb_0 = digital.map_bb([down_const.pre_diff_code().index(i) for i in range(len(down_const.pre_diff_code()))])
        self.digital_diff_encoder_bb_0_0_0 = digital.diff_encoder_bb(len(down_const.points()))
        self.digital_constellation_receiver_cb_0_0 = digital.constellation_receiver_cb(bpsk_const, 62800000, 0, 1024)
        self.digital_constellation_receiver_cb_0 = digital.constellation_receiver_cb(up_const, 62800000, 0, 1024)
        self.bpsk_pass_sim_channel = leo.channel_model(bpsk_samp_rate, down_pass_sim_model, 0)
        self.blocks_unpack_k_bits_bb_1_0_0_0_0_1 = blocks.unpack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_unpack_k_bits_bb_1_0 = blocks.unpack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_unpack_k_bits_bb_0_2 = blocks.unpack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_unpack_k_bits_bb_0_1_0 = blocks.unpack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_unpack_k_bits_bb_0_0_0 = blocks.unpack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_selector_0_0 = blocks.selector(gr.sizeof_char*1,0,0)
        self.blocks_selector_0_0.set_enabled(True)
        self.blocks_selector_0 = blocks.selector(gr.sizeof_char*1,0,0)
        self.blocks_selector_0.set_enabled(False)
        self.blocks_packed_to_unpacked_xx_0_0_0 = blocks.packed_to_unpacked_bb(down_const.bits_per_symbol(), gr.GR_MSB_FIRST)
        self.blocks_pack_k_bits_bb_1_0_0_0_0_1 = blocks.pack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_pack_k_bits_bb_1_0 = blocks.pack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_pack_k_bits_bb_0_0 = blocks.pack_k_bits_bb(down_const.bits_per_symbol())
        self.blocks_null_sink_1_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_1 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_2 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_1 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_float*1)
        self.blocks_multiply_const_vxx_0_0_0_0_0_1 = blocks.multiply_const_ff(2)
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_ff(2)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, '/home/holeczcc/gnuradio/transfer/GUI/RawCadu.raw', True, 0, 0)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_char_to_float_0_1_0_0_0_1 = blocks.char_to_float(1, 1)
        self.blocks_char_to_float_0_1 = blocks.char_to_float(1, 1)
        self.blocks_add_const_vxx_0_0_0_0_0_1 = blocks.add_const_ff(-1)
        self.blocks_add_const_vxx_0_0 = blocks.add_const_ff(-1)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.bpsk_pass_sim_channel, 'csv'), (self.zeromq_pub_msg_sink_0, 'in'))
        self.msg_connect((self.down_asm_sync, 'lock'), (self.zeromq_pub_msg_sink_0_0, 'in'))
        self.msg_connect((self.down_asm_sync_0, 'lock'), (self.zeromq_pub_msg_sink_0_0_0_0_0, 'in'))
        self.msg_connect((self.down_stat_frame_lock, 'lock'), (self.zeromq_pub_msg_sink_0_0_0_0, 'in'))
        self.msg_connect((self.down_stat_frame_lock_0, 'lock'), (self.zeromq_pub_msg_sink_0_0_1, 'in'))
        self.msg_connect((self.down_stat_frame_lock_1_0, 'lock'), (self.zeromq_pub_msg_sink_0_0_1_0, 'in'))
        self.msg_connect((self.down_stat_snr, 'snr'), (self.zeromq_pub_msg_sink_0_0_0, 'in'))
        self.msg_connect((self.down_stat_snr_0, 'snr'), (self.zeromq_pub_msg_sink_0_0_0_1, 'in'))
        self.msg_connect((self.down_stat_snr_1, 'snr'), (self.zeromq_pub_msg_sink_0_0_0_1_0, 'in'))
        self.msg_connect((self.rf_scoe_asm_sync_bb_0, 'lock'), (self.zeromq_pub_msg_sink_0_0_0_0_0_0, 'in'))
        self.msg_connect((self.up_pass_sim_channel, 'csv'), (self.zeromq_pub_msg_sink_0_1, 'in'))
        self.msg_connect((self.up_pass_sim_channel_1, 'csv'), (self.zeromq_pub_msg_sink_0_0_0_2, 'in'))
        self.msg_connect((self.zeromq_sub_msg_source_0, 'out'), (self.blocks_selector_0_0, 'en'))
        self.msg_connect((self.zeromq_sub_msg_source_0_0, 'out'), (self.blocks_selector_0, 'en'))
        self.connect((self.blocks_add_const_vxx_0_0, 0), (self.down_fec_dec_0_0, 0))
        self.connect((self.blocks_add_const_vxx_0_0_0_0_0_1, 0), (self.down_fec_dec_0_0_0_0_1, 0))
        self.connect((self.blocks_char_to_float_0_1, 0), (self.blocks_multiply_const_vxx_0_0, 0))
        self.connect((self.blocks_char_to_float_0_1_0_0_0_1, 0), (self.blocks_multiply_const_vxx_0_0_0_0_0_1, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_selector_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_add_const_vxx_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0_0_0_0_0_1, 0), (self.blocks_add_const_vxx_0_0_0_0_0_1, 0))
        self.connect((self.blocks_pack_k_bits_bb_0_0, 0), (self.up_const_mod_2, 0))
        self.connect((self.blocks_pack_k_bits_bb_1_0, 0), (self.down_dif_dec_0_1, 0))
        self.connect((self.blocks_pack_k_bits_bb_1_0_0_0_0_1, 0), (self.down_dif_dec_0_0_0_0_1, 0))
        self.connect((self.blocks_packed_to_unpacked_xx_0_0_0, 0), (self.digital_map_bb_1_0_0, 0))
        self.connect((self.blocks_selector_0, 0), (self.blocks_packed_to_unpacked_xx_0_0_0, 0))
        self.connect((self.blocks_selector_0_0, 0), (self.up_unpack, 0))
        self.connect((self.blocks_unpack_k_bits_bb_0_0_0, 0), (self.up_fec_enc_0_0, 0))
        self.connect((self.blocks_unpack_k_bits_bb_0_1_0, 0), (self.blocks_char_to_float_0_1, 0))
        self.connect((self.blocks_unpack_k_bits_bb_0_2, 0), (self.blocks_char_to_float_0_1_0_0_0_1, 0))
        self.connect((self.blocks_unpack_k_bits_bb_1_0, 0), (self.rf_scoe_asm_sync_bb_0, 1))
        self.connect((self.blocks_unpack_k_bits_bb_1_0_0_0_0_1, 0), (self.rf_scoe_asm_sync_bb_0, 0))
        self.connect((self.bpsk_pass_sim_channel, 0), (self.sim_throttle, 0))
        self.connect((self.digital_constellation_receiver_cb_0, 1), (self.blocks_null_sink_0_0_0, 0))
        self.connect((self.digital_constellation_receiver_cb_0, 2), (self.blocks_null_sink_0_1, 0))
        self.connect((self.digital_constellation_receiver_cb_0, 3), (self.blocks_null_sink_1_0, 0))
        self.connect((self.digital_constellation_receiver_cb_0, 0), (self.down_dif_dec_0_0, 0))
        self.connect((self.digital_constellation_receiver_cb_0, 4), (self.down_stat_snr_0, 0))
        self.connect((self.digital_constellation_receiver_cb_0_0, 2), (self.blocks_null_sink_0, 0))
        self.connect((self.digital_constellation_receiver_cb_0_0, 1), (self.blocks_null_sink_0_0, 0))
        self.connect((self.digital_constellation_receiver_cb_0_0, 3), (self.blocks_null_sink_1, 0))
        self.connect((self.digital_constellation_receiver_cb_0_0, 0), (self.down_const_null, 0))
        self.connect((self.digital_constellation_receiver_cb_0_0, 4), (self.down_const_soft_dec, 0))
        self.connect((self.digital_constellation_receiver_cb_0_0, 4), (self.down_stat_snr, 0))
        self.connect((self.digital_diff_encoder_bb_0_0_0, 0), (self.blocks_unpack_k_bits_bb_0_0_0, 0))
        self.connect((self.digital_map_bb_0, 0), (self.blocks_unpack_k_bits_bb_1_0, 0))
        self.connect((self.digital_map_bb_0_0_0_0_1, 0), (self.blocks_unpack_k_bits_bb_1_0_0_0_0_1, 0))
        self.connect((self.digital_map_bb_1_0_0, 0), (self.digital_diff_encoder_bb_0_0_0, 0))
        self.connect((self.digital_map_bb_1_1_0, 0), (self.blocks_unpack_k_bits_bb_0_1_0, 0))
        self.connect((self.down_asm_sync, 0), (self.down_stat_frame_lock, 0))
        self.connect((self.down_asm_sync_0, 0), (self.down_stat_frame_lock_0, 0))
        self.connect((self.down_const_recv, 2), (self.blocks_null_sink_0_2, 1))
        self.connect((self.down_const_recv, 1), (self.blocks_null_sink_0_2, 0))
        self.connect((self.down_const_recv, 3), (self.blocks_null_sink_0_2, 2))
        self.connect((self.down_const_recv, 0), (self.blocks_unpack_k_bits_bb_0_2, 0))
        self.connect((self.down_const_recv, 0), (self.digital_map_bb_1_1_0, 0))
        self.connect((self.down_const_recv, 0), (self.down_const_null_0, 0))
        self.connect((self.down_const_recv, 4), (self.down_stat_snr_1, 0))
        self.connect((self.down_const_soft_dec, 0), (self.down_fec_dec_0, 0))
        self.connect((self.down_const_soft_dec, 0), (self.down_fec_delay, 0))
        self.connect((self.down_dif_dec_0, 0), (self.down_asm_sync, 0))
        self.connect((self.down_dif_dec_0_0, 0), (self.down_asm_sync_0, 1))
        self.connect((self.down_dif_dec_0_0, 0), (self.down_asm_sync_0, 0))
        self.connect((self.down_dif_dec_0_0_0_0_1, 0), (self.digital_map_bb_0_0_0_0_1, 0))
        self.connect((self.down_dif_dec_0_1, 0), (self.digital_map_bb_0, 0))
        self.connect((self.down_dif_dec_1, 0), (self.down_asm_sync, 1))
        self.connect((self.down_fec_dec_0, 0), (self.down_dif_dec_0, 0))
        self.connect((self.down_fec_dec_0_0, 0), (self.blocks_pack_k_bits_bb_1_0, 0))
        self.connect((self.down_fec_dec_0_0_0_0_1, 0), (self.blocks_pack_k_bits_bb_1_0_0_0_0_1, 0))
        self.connect((self.down_fec_dec_1, 0), (self.down_dif_dec_1, 0))
        self.connect((self.down_fec_delay, 0), (self.down_fec_dec_1, 0))
        self.connect((self.down_sym_sync, 0), (self.digital_constellation_receiver_cb_0_0, 0))
        self.connect((self.down_sym_sync_0, 0), (self.digital_constellation_receiver_cb_0, 0))
        self.connect((self.down_sym_sync_1, 0), (self.down_const_recv, 0))
        self.connect((self.file_source, 0), (self.blocks_selector_0_0, 0))
        self.connect((self.file_source_0, 0), (self.up_unpack_0, 0))
        self.connect((self.pfb_arb_resampler_xxx_0_0_0, 0), (self.up_pass_sim_channel_1, 0))
        self.connect((self.rf_scoe_asm_sync_bb_0, 0), (self.down_stat_frame_lock_1_0, 0))
        self.connect((self.sim_throttle, 0), (self.down_sym_sync, 0))
        self.connect((self.sim_throttle_0, 0), (self.down_sym_sync_0, 0))
        self.connect((self.up_const_mod, 0), (self.bpsk_pass_sim_channel, 0))
        self.connect((self.up_const_mod_0, 0), (self.up_pass_sim_channel, 0))
        self.connect((self.up_const_mod_2, 0), (self.pfb_arb_resampler_xxx_0_0_0, 0))
        self.connect((self.up_dif_enc, 0), (self.up_fec_enc, 0))
        self.connect((self.up_dif_enc_0_0, 0), (self.up_pack_0, 0))
        self.connect((self.up_fec_enc, 0), (self.up_pack, 0))
        self.connect((self.up_fec_enc_0_0, 0), (self.blocks_pack_k_bits_bb_0_0, 0))
        self.connect((self.up_pack, 0), (self.up_const_mod, 0))
        self.connect((self.up_pack_0, 0), (self.up_const_mod_0, 0))
        self.connect((self.up_pass_sim_channel, 0), (self.sim_throttle_0, 0))
        self.connect((self.up_pass_sim_channel_1, 0), (self.down_sym_sync_1, 0))
        self.connect((self.up_unpack, 0), (self.up_dif_enc, 0))
        self.connect((self.up_unpack_0, 0), (self.up_dif_enc_0_0, 0))

    def get_up_center_freq(self):
        return self.up_center_freq

    def set_up_center_freq(self, up_center_freq):
        self.up_center_freq = up_center_freq

    def get_bpsk_center_freq(self):
        return self.bpsk_center_freq

    def set_bpsk_center_freq(self, bpsk_center_freq):
        self.bpsk_center_freq = bpsk_center_freq

    def get_pass_sim_sat_tx_ant(self):
        return self.pass_sim_sat_tx_ant

    def set_pass_sim_sat_tx_ant(self, pass_sim_sat_tx_ant):
        self.pass_sim_sat_tx_ant = pass_sim_sat_tx_ant
        self.pass_sim_sat_tx_ant.set_pointing_error(0)

    def get_pass_sim_sat_rx_ant(self):
        return self.pass_sim_sat_rx_ant

    def set_pass_sim_sat_rx_ant(self, pass_sim_sat_rx_ant):
        self.pass_sim_sat_rx_ant = pass_sim_sat_rx_ant
        self.pass_sim_sat_rx_ant.set_pointing_error(0)

    def get_pass_sim_sat(self):
        return self.pass_sim_sat

    def set_pass_sim_sat(self, pass_sim_sat):
        self.pass_sim_sat = pass_sim_sat

    def get_pass_sim_gs_tx_ant(self):
        return self.pass_sim_gs_tx_ant

    def set_pass_sim_gs_tx_ant(self, pass_sim_gs_tx_ant):
        self.pass_sim_gs_tx_ant = pass_sim_gs_tx_ant
        self.pass_sim_gs_tx_ant.set_pointing_error(0.5)

    def get_pass_sim_gs_rx_ant(self):
        return self.pass_sim_gs_rx_ant

    def set_pass_sim_gs_rx_ant(self, pass_sim_gs_rx_ant):
        self.pass_sim_gs_rx_ant = pass_sim_gs_rx_ant
        self.pass_sim_gs_rx_ant.set_pointing_error(0.5)

    def get_up_symb_rate(self):
        return self.up_symb_rate

    def set_up_symb_rate(self, up_symb_rate):
        self.up_symb_rate = up_symb_rate
        self.set_up_samp_rate(self.up_symb_rate * self.up_sps)

    def get_up_sps(self):
        return self.up_sps

    def set_up_sps(self, up_sps):
        self.up_sps = up_sps
        self.set_up_rrc_taps(firdes.root_raised_cosine(self.up_nfilts, self.up_nfilts, 1.0/float(self.up_sps), self.up_rolloff, 45*self.up_nfilts))
        self.set_up_samp_rate(self.up_symb_rate * self.up_sps)

    def get_up_rolloff(self):
        return self.up_rolloff

    def set_up_rolloff(self, up_rolloff):
        self.up_rolloff = up_rolloff
        self.set_up_rrc_taps(firdes.root_raised_cosine(self.up_nfilts, self.up_nfilts, 1.0/float(self.up_sps), self.up_rolloff, 45*self.up_nfilts))

    def get_up_nfilts(self):
        return self.up_nfilts

    def set_up_nfilts(self, up_nfilts):
        self.up_nfilts = up_nfilts
        self.set_up_rrc_taps(firdes.root_raised_cosine(self.up_nfilts, self.up_nfilts, 1.0/float(self.up_sps), self.up_rolloff, 45*self.up_nfilts))

    def get_pass_sim_gs(self):
        return self.pass_sim_gs

    def set_pass_sim_gs(self, pass_sim_gs):
        self.pass_sim_gs = pass_sim_gs

    def get_bpsk_symb_rate(self):
        return self.bpsk_symb_rate

    def set_bpsk_symb_rate(self, bpsk_symb_rate):
        self.bpsk_symb_rate = bpsk_symb_rate
        self.set_bpsk_samp_rate(self.bpsk_symb_rate * self.bpsk_sps)

    def get_bpsk_sps(self):
        return self.bpsk_sps

    def set_bpsk_sps(self, bpsk_sps):
        self.bpsk_sps = bpsk_sps
        self.set_bpsk_rrc_taps(firdes.root_raised_cosine(self.bpsk_nfilts, self.bpsk_nfilts, 1.0/float(self.bpsk_sps), self.bpsk_rolloff, 45*self.bpsk_nfilts))
        self.set_bpsk_samp_rate(self.bpsk_symb_rate * self.bpsk_sps)
        self.pfb_arb_resampler_xxx_0_0_0.set_rate(self.bpsk_sps)

    def get_bpsk_rolloff(self):
        return self.bpsk_rolloff

    def set_bpsk_rolloff(self, bpsk_rolloff):
        self.bpsk_rolloff = bpsk_rolloff
        self.set_bpsk_rrc_taps(firdes.root_raised_cosine(self.bpsk_nfilts, self.bpsk_nfilts, 1.0/float(self.bpsk_sps), self.bpsk_rolloff, 45*self.bpsk_nfilts))

    def get_bpsk_nfilts(self):
        return self.bpsk_nfilts

    def set_bpsk_nfilts(self, bpsk_nfilts):
        self.bpsk_nfilts = bpsk_nfilts
        self.set_bpsk_rrc_taps(firdes.root_raised_cosine(self.bpsk_nfilts, self.bpsk_nfilts, 1.0/float(self.bpsk_sps), self.bpsk_rolloff, 45*self.bpsk_nfilts))

    def get_up_samp_rate(self):
        return self.up_samp_rate

    def set_up_samp_rate(self, up_samp_rate):
        self.up_samp_rate = up_samp_rate
        self.sim_throttle_0.set_sample_rate(self.up_samp_rate)

    def get_up_rrc_taps(self):
        return self.up_rrc_taps

    def set_up_rrc_taps(self, up_rrc_taps):
        self.up_rrc_taps = up_rrc_taps

    def get_up_pass_sim_model(self):
        return self.up_pass_sim_model

    def set_up_pass_sim_model(self, up_pass_sim_model):
        self.up_pass_sim_model = up_pass_sim_model

    def get_up_conv_enc(self):
        return self.up_conv_enc

    def set_up_conv_enc(self, up_conv_enc):
        self.up_conv_enc = up_conv_enc

    def get_up_const(self):
        return self.up_const

    def set_up_const(self, up_const):
        self.up_const = up_const

    def get_down_pass_sim_model(self):
        return self.down_pass_sim_model

    def set_down_pass_sim_model(self, down_pass_sim_model):
        self.down_pass_sim_model = down_pass_sim_model

    def get_down_conv_dec_1_0(self):
        return self.down_conv_dec_1_0

    def set_down_conv_dec_1_0(self, down_conv_dec_1_0):
        self.down_conv_dec_1_0 = down_conv_dec_1_0

    def get_down_conv_dec_1(self):
        return self.down_conv_dec_1

    def set_down_conv_dec_1(self, down_conv_dec_1):
        self.down_conv_dec_1 = down_conv_dec_1

    def get_down_const(self):
        return self.down_const

    def set_down_const(self, down_const):
        self.down_const = down_const

    def get_bpsk_samp_rate(self):
        return self.bpsk_samp_rate

    def set_bpsk_samp_rate(self, bpsk_samp_rate):
        self.bpsk_samp_rate = bpsk_samp_rate
        self.sim_throttle.set_sample_rate(self.bpsk_samp_rate)

    def get_bpsk_rrc_taps(self):
        return self.bpsk_rrc_taps

    def set_bpsk_rrc_taps(self, bpsk_rrc_taps):
        self.bpsk_rrc_taps = bpsk_rrc_taps
        self.pfb_arb_resampler_xxx_0_0_0.set_taps(self.bpsk_rrc_taps)

    def get_bpsk_conv_enc(self):
        return self.bpsk_conv_enc

    def set_bpsk_conv_enc(self, bpsk_conv_enc):
        self.bpsk_conv_enc = bpsk_conv_enc

    def get_bpsk_conv_dec_1(self):
        return self.bpsk_conv_dec_1

    def set_bpsk_conv_dec_1(self, bpsk_conv_dec_1):
        self.bpsk_conv_dec_1 = bpsk_conv_dec_1

    def get_bpsk_conv_dec_0(self):
        return self.bpsk_conv_dec_0

    def set_bpsk_conv_dec_0(self, bpsk_conv_dec_0):
        self.bpsk_conv_dec_0 = bpsk_conv_dec_0

    def get_bpsk_const(self):
        return self.bpsk_const

    def set_bpsk_const(self, bpsk_const):
        self.bpsk_const = bpsk_const

    def set_bpsk(self):
        self.blocks_selector_0_0.set_enabled(True)
        self.blocks_selector_0.set_enabled(False)

    def set_qpsk(self):
        self.blocks_selector_0_0.set_enabled(False)
        self.blocks_selector_0.set_enabled(True)

    def set_coderate(self, code):
        if code == 2:
            code = '11'
        elif code == 3:
            code = '1101'
        elif code == 4:
            code = '110110'
        elif code == 6:
            code = '1101100110'
        else:
            code = '11010101100110'
        self.down_fec_dec_1 = fec.extended_decoder(decoder_obj_list=self.bpsk_conv_dec_1, threading= None, ann=None, puncpat=code, integration_period=10000)
        self.down_fec_dec_0_0_0_0_1 = fec.extended_decoder(decoder_obj_list=self.down_conv_dec_1, threading= None, ann=None, puncpat=code, integration_period=10000)
        self.down_fec_dec_0_0 = fec.extended_decoder(decoder_obj_list=self.down_conv_dec_1_0, threading= None, ann=None, puncpat=code, integration_period=10000)
        self.down_fec_dec_0 = fec.extended_decoder(decoder_obj_list=self.bpsk_conv_dec_0, threading= None, ann=None, puncpat=code, integration_period=10000)
        self.up_fec_enc_0_0 = fec.extended_encoder(encoder_obj_list=self.up_conv_enc, threading= None, puncpat=code)
        self.up_fec_enc = fec.extended_encoder(encoder_obj_list=self.bpsk_conv_enc, threading= None, puncpat=code)

def main(top_block_cls=rf_scoe_autogen, options=None):
    tb = top_block_cls()

    UDPSock = socket(AF_INET, SOCK_DGRAM)
    UDPSock.bind(("127.0.0.1", 6666))

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    tb.set_coderate(2)
    tb.set_bpsk_symb_rate(128000)
    tb.set_bpsk()
    tb.start()

    while(True):
        data, addr = UDPSock.recvfrom(1024)
        data = json.loads(data.decode())
        mod = data.get("mod")
        symrate = data.get("symrate")
        coderate = data.get("coderate")
        tb.lock()
        tb.set_coderate(coderate)
        tb.set_bpsk_symb_rate(symrate)
        if mod==4:
            tb.set_qpsk()
        else:
            tb.set_bpsk()
        #tb.set_Coderate(coderate)
        tb.unlock()


if __name__ == '__main__':
    main()