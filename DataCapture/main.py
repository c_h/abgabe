import math
import queue
import struct
import zmq
import pmt
import csv
from multiprocessing import Process, Queue
from socket import *
import json
from datetime import datetime


def readDownBPSKOverflight(queueBPSKSlantRange, queueBPSKElevation, queueBPSKPathLoss, queueBPSKAtmoLoss,
                           queueBPSKRainLoss, queueBPSKLinkMargin, queueBPSKTime):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:3330")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            data = msg.decode()
            data = data[18:]
            dataSplit = data.split(",")
            queueBPSKTime.put(dataSplit[0][:-4])
            queueBPSKSlantRange.put(float(dataSplit[1]))
            queueBPSKElevation.put(float(dataSplit[2]))
            queueBPSKPathLoss.put(float(dataSplit[3]))
            queueBPSKAtmoLoss.put(float(dataSplit[4]))
            queueBPSKRainLoss.put(float(dataSplit[5]))
            queueBPSKLinkMargin.put(float(dataSplit[9]))
        except UnicodeDecodeError:
            pass


def readDownBPSKSNR(queueBPSKSNR):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:3331")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    UDPSockS = socket(AF_INET, SOCK_DGRAM)
    tempQueue = Queue(maxsize=5)
    for i in range(0, 5):
        tempQueue.put(float(0))
    while (True):
        msg = zmq_sock.recv()
        tempVal = 0
        recvValue = float(pmt.to_python(pmt.deserialize_str(msg)))
        if not math.isnan(recvValue):
            if recvValue > 0:
                tempQueue.get()
                tempQueue.put(recvValue)
                tempVal = 0
                tempList = []
                for i in range(0, 5):
                    tempList.append(tempQueue.get())
                    tempVal += tempList[i]
                for i in range(0, 5):
                    tempQueue.put(tempList[i])
                tempVal = tempVal / 5
                if tempVal < 4.5:
                    UDPSockS.sendto(struct.pack('!f', tempVal), ("127.0.0.1", 6667))
        queueBPSKSNR.put([pmt.to_python(pmt.deserialize_str(msg)), tempVal])


def readDownBPSKASM(queueBPSKASM):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:3333")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            if "unlocked" not in msg.decode():
                queueBPSKASM.put("locked")
            else:
                queueBPSKASM.put("unlocked")
        except UnicodeDecodeError:
            pass


def readDownBPSKFrameLock(queueBPSKFrame):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:3334")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            if "unlocked" not in msg.decode():
                queueBPSKFrame.put("locked")
            else:
                queueBPSKFrame.put("unlocked")
        except UnicodeDecodeError:
            pass


def readUpOverflight(queueUpSlantRange, queueUpElevation, queueUpPathLoss, queueUpAtmoLoss, queueUpRainLoss,
                     queueUpLinkMargin, queueUpTime, queueTimeDiff):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:4440")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    timeOld = datetime.strptime('20:29:34.0000', '%H:%M:%S.%f')
    timeNew = 0
    while (True):
        msg = zmq_sock.recv()
        try:
            data = msg.decode()
            data = data[18:]
            dataSplit = data.split(",")
            queueUpTime.put(dataSplit[0][:-4])
            timeNew = datetime.strptime(dataSplit[0][:-4], ' %H:%M:%S.%f')
            timediff = timeNew - timeOld
            timeOld = timeNew
            queueTimeDiff.put(timediff)
            queueUpSlantRange.put(float(dataSplit[1]))
            queueUpElevation.put(float(dataSplit[2]))
            queueUpPathLoss.put(float(dataSplit[3]))
            queueUpAtmoLoss.put(float(dataSplit[4]))
            queueUpRainLoss.put(float(dataSplit[5]))
            queueUpLinkMargin.put(float(dataSplit[9]))
            UDPSock = socket(AF_INET, SOCK_DGRAM)
            if (float(dataSplit[2]) > 10 and float(dataSplit[2]) < 20):
                tempJson = json.dumps({"mod": 2, "coderate": 2,
                                       "symrate": 240000}).encode('utf8')
            elif float(dataSplit[2]) > 20 and float(dataSplit[2]) < 30:
                tempJson = json.dumps({"mod": 2, "coderate": 2,
                                       "symrate": 470000}).encode('utf8')
            elif float(dataSplit[2]) > 30 and float(dataSplit[2]) < 40:
                tempJson = json.dumps({"mod": 2, "coderate": 2,
                                       "symrate": 780000}).encode('utf8')
            elif float(dataSplit[2]) > 40 and float(dataSplit[2]) < 50:
                tempJson = json.dumps({"mod": 2, "coderate": 2,
                                       "symrate": 1160000}).encode('utf8')
            elif float(dataSplit[2]) > 50:
                tempJson = json.dumps({"mod": 2, "coderate": 2,
                                       "symrate": 1500000}).encode('utf8')
            else:
                tempJson = json.dumps({"mod": 2, "coderate": 2,
                                       "symrate": 128000}).encode('utf8')
            UDPSock.sendto(tempJson, ("127.0.0.1", 6665))
        except UnicodeDecodeError:
            pass



def readUpSNR(queueUpSNR):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:4441")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    UDPSockS = socket(AF_INET, SOCK_DGRAM)
    tempQueue = Queue(maxsize=10)
    for i in range(0, 10):
        tempQueue.put(float(0))
    while (True):
        msg = zmq_sock.recv()
        recvValue = float(pmt.to_python(pmt.deserialize_str(msg)))
        if not math.isnan(recvValue):
            if recvValue > 0:
                tempQueue.get()
                tempQueue.put(recvValue)
                tempVal = 0
                tempList = []
                for i in range(0, 10):
                    tempList.append(tempQueue.get())
                    tempVal += tempList[i]
                for i in range(0, 10):
                    tempQueue.put(tempList[i])
                tempVal = tempVal/10
                UDPSockS.sendto(struct.pack('!f', tempVal), ("127.0.0.1", 6664))
                queueUpSNR.put([pmt.to_python(pmt.deserialize_str(msg)), tempVal])


def readUpASM(queueUpASM):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:4443")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            if "unlocked" not in msg.decode():
                queueUpASM.put("locked")
            else:
                queueUpASM.put("unlocked")
        except UnicodeDecodeError:
            pass


def readUpFrameLock(queueUpFrame):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:4444")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            if "unlocked" not in msg.decode():
                queueUpFrame.put("locked")
            else:
                queueUpFrame.put("unlocked")
        except UnicodeDecodeError:
            pass


def readDownQPSKOverflight(queueQPSKSlantRange, queueQPSKElevation, queueQPSKPathLoss, queueQPSKAtmoLoss,
                           queueQPSKRainLoss, queueQPSKLinkMargin, queueQPSKTime):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:5550")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            data = msg.decode()
            data = data[18:]
            dataSplit = data.split(",")
            queueQPSKTime.put(dataSplit[0][:-4])
            queueQPSKSlantRange.put(float(dataSplit[1]))
            queueQPSKElevation.put(float(dataSplit[2]))
            queueQPSKPathLoss.put(float(dataSplit[3]))
            queueQPSKAtmoLoss.put(float(dataSplit[4]))
            queueQPSKRainLoss.put(float(dataSplit[5]))
            queueQPSKLinkMargin.put(float(dataSplit[9]))
        except UnicodeDecodeError:
            pass


def readDownQPSKSNR(queueQPSKSNR):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:5551")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    UDPSockS = socket(AF_INET, SOCK_DGRAM)
    while (True):
        msg = zmq_sock.recv()
        recvValue = float(pmt.to_python(pmt.deserialize_str(msg)))
        if not math.isnan(recvValue):
            if recvValue < 4.5:
                UDPSockS.sendto(struct.pack('!f', recvValue), ("127.0.0.1", 6667))
        queueQPSKSNR.put(pmt.to_python(pmt.deserialize_str(msg)))


def readDownQPSKASM(queueQPSKASM):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:5553")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            if "unlocked" not in msg.decode():
                queueQPSKASM.put("locked")
            else:
                queueQPSKASM.put("unlocked")
        except UnicodeDecodeError:
            pass


def readDownQPSKFrameLock(queueQPSKFrame):
    zmq_context = zmq.Context()
    zmq_sock = zmq_context.socket(zmq.SUB)
    zmq_sock.connect("tcp://127.0.0.1:5554")
    zmq_sock.setsockopt(zmq.SUBSCRIBE, b'')
    while (True):
        msg = zmq_sock.recv()
        try:
            if "unlocked" not in msg.decode():
                queueQPSKFrame.put("locked")
            else:
                queueQPSKFrame.put("unlocked")
        except UnicodeDecodeError:
            pass

def processorConfig(queueConfig):
    UDPSockR = socket(AF_INET,SOCK_DGRAM)
    UDPSockR.bind(("127.0.0.1", 6665))
    while(True):
        data, addr = UDPSockR.recvfrom(1024)
        UDPSockR.sendto(data, ("127.0.0.1", 6666))
        data = json.loads(data.decode())
        mod = data.get("mod")
        symrate = data.get("symrate")
        coderate = data.get("coderate")
        #exploit = data.get("exploit")
        #fit = data.get("fit")
        #train = data.get("train")
        #time = data.get("time")
        #expl = data.get("expl")
        #queueConfig.put([mod, symrate, coderate, exploit, fit, train, time, expl])
        queueConfig.put([mod, symrate, coderate])

if __name__ == '__main__':

    # Variables
    dataBPSKSlantRange = float(0)
    dataBPSKElevation = float(0)
    dataBPSKPathLoss = float(0)
    dataBPSKAtmoLoss = float(0)
    dataBPSKRainLoss = float(0)
    dataBPSKLinkMargin = float(0)
    dataQPSKSlantRange = float(0)
    dataQPSKElevation = float(0)
    dataQPSKPathLoss = float(0)
    dataQPSKAtmoLoss = float(0)
    dataQPSKRainLoss = float(0)
    dataQPSKLinkMargin = float(0)
    dataUpSlantRange = float(0)
    dataUpElevation = float(0)
    dataUpPathLoss = float(0)
    dataUpAtmoLoss = float(0)
    dataUpRainLoss = float(0)
    dataUpLinkMargin = float(0)
    dataUpTime = ""
    dataBPSKTime = ""
    dataQPSKTime = ""
    dataUpASM = ""
    dataUpFrame = ""
    dataUpSNR = float(0)
    dataBPSKASM = ""
    dataBPSKFrame = ""
    dataBPSKSNR = float(0)
    dataQPSKASM = ""
    dataQPSKFrame = ""
    dataQPSKSNR = float(0)
    dataConfig = []
    dataTimeDiff = ""

    # Queues
    queueBPSKSlantRange = Queue()
    queueBPSKElevation = Queue()
    queueBPSKPathLoss = Queue()
    queueBPSKAtmoLoss = Queue()
    queueBPSKRainLoss = Queue()
    queueBPSKLinkMargin = Queue()
    queueQPSKSlantRange = Queue()
    queueQPSKElevation = Queue()
    queueQPSKPathLoss = Queue()
    queueQPSKAtmoLoss = Queue()
    queueQPSKRainLoss = Queue()
    queueQPSKLinkMargin = Queue()
    queueUpSlantRange = Queue()
    queueUpElevation = Queue()
    queueUpPathLoss = Queue()
    queueUpAtmoLoss = Queue()
    queueUpRainLoss = Queue()
    queueUpLinkMargin = Queue()
    queueUpTime = Queue()
    queueBPSKTime = Queue()
    queueQPSKTime = Queue()
    queueUpASM = Queue()
    queueUpFrame = Queue()
    queueUpSNR = Queue()
    queueBPSKASM = Queue()
    queueBPSKFrame = Queue()
    queueBPSKSNR = Queue()
    queueQPSKASM = Queue()
    queueQPSKFrame = Queue()
    queueQPSKSNR = Queue()
    queueConfig = Queue()
    queueTimeDiff = Queue()

    # Processes
    processBPSK = Process(target=readDownBPSKOverflight, args=(
        queueBPSKSlantRange, queueBPSKElevation, queueBPSKPathLoss, queueBPSKAtmoLoss, queueBPSKRainLoss,
        queueBPSKLinkMargin, queueBPSKTime,))
    processQPSK = Process(target=readDownQPSKOverflight, args=(
        queueQPSKSlantRange, queueQPSKElevation, queueQPSKPathLoss, queueQPSKAtmoLoss, queueQPSKRainLoss,
        queueQPSKLinkMargin, queueQPSKTime,))
    processUp = Process(target=readUpOverflight, args=(
        queueUpSlantRange, queueUpElevation, queueUpPathLoss, queueUpAtmoLoss, queueUpRainLoss, queueUpLinkMargin,
        queueUpTime,queueTimeDiff))
    processUpASM = Process(target=readUpASM, args=(queueUpASM,))
    processUpFrame = Process(target=readUpFrameLock, args=(queueUpFrame,))
    processUpSNR = Process(target=readUpSNR, args=(queueUpSNR,))
    processBPSKASM = Process(target=readDownBPSKASM, args=(queueBPSKASM,))
    processBPSKFrame = Process(target=readDownBPSKFrameLock, args=(queueBPSKFrame,))
    processBPSKSNR = Process(target=readDownBPSKSNR, args=(queueBPSKSNR,))
    processQPSKASM = Process(target=readDownQPSKASM, args=(queueQPSKASM,))
    processQPSKFrame = Process(target=readDownQPSKFrameLock, args=(queueQPSKFrame,))
    processQPSKSNR = Process(target=readDownQPSKSNR, args=(queueQPSKSNR,))
    processConfig = Process(target=processorConfig, args=(queueConfig,))

    inputFilename = input("Enter Filename: ")
    filename = "/home/holeczcc/code/CapturedData/"
    filename += inputFilename
    filename += ".csv"
    print("Your Data is saved in: " + filename + "\n")
    file = open(filename, 'w', encoding='UTF-8')
    filewriter = csv.writer(file)
    filewriter.writerow(
        ["SysTime", "BPSKTime", "BPSKSlantRange", "BPSKElevation", "BPSKPathLoss", "BPSKLinkMargin", "BPSKRainLoss",
         "BPSKAtmoLoss", "BPSKASM", "BPSKFrame", "BPSKSNR", "QPSKTime", "QPSKSlantRange", "QPSKElevation",
         "QPSKPathLoss", "QPSKLinkMargin", "QPSKRainLoss",
         "QPSKAtmoLoss", "QPSKASM", "QPSKFrame", "QPSKSNR", "UpTime", "UpSlantRange", "UpElevation", "UpPathLoss",
         "UpLinkMargin", "UpRainLoss",
         "UpAtmoLoss", "UpASM", "UpFrame", "UpSNR", "Config"])
    print("SysTime, BPSKTime, BPSKSlantRange, BPSKElevation, BPSKPathLoss, BPSKLinkMargin, BPSKRainLoss, BPSKAtmoLoss, BPSKASM, BPSKFrame, BPSKSNR, QPSKTime, QPSKSlantRange, QPSKElevation, QPSKPathLoss, QPSKLinkMargin, QPSKRainLoss, QPSKAtmoLoss, QPSKASM, QPSKFrame, QPSKSNR, UpTime, UpSlantRange, UpElevation, UpPathLoss, UpLinkMargin, UpRainLoss, UpAtmoLoss, UpASM, UpFrame, UpSNR, Config")
    file.flush()
    processBPSK.start()
    processQPSK.start()
    processUp.start()
    processUpASM.start()
    processUpFrame.start()
    processUpSNR.start()
    processBPSKASM.start()
    processBPSKFrame.start()
    processBPSKSNR.start()
    processQPSKASM.start()
    processQPSKFrame.start()
    processQPSKSNR.start()
    processConfig.start()
    dataOld = ""
    transferData = 0
    while (True):
        try:
            dataBPSKSlantRange = queueBPSKSlantRange.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKElevation = queueBPSKElevation.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKPathLoss = queueBPSKPathLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKAtmoLoss = queueBPSKAtmoLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKRainLoss = queueBPSKRainLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKLinkMargin = queueBPSKLinkMargin.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKTime = queueBPSKTime.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKASM = queueBPSKASM.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKFrame = queueBPSKFrame.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataBPSKSNR = queueBPSKSNR.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty

        try:
            dataQPSKSlantRange = queueQPSKSlantRange.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKElevation = queueQPSKElevation.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKPathLoss = queueQPSKPathLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKAtmoLoss = queueQPSKAtmoLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKRainLoss = queueQPSKRainLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKLinkMargin = queueQPSKLinkMargin.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKTime = queueQPSKTime.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKASM = queueQPSKASM.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKFrame = queueQPSKFrame.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataQPSKSNR = queueQPSKSNR.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty

        try:
            dataUpSlantRange = queueUpSlantRange.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpElevation = queueUpElevation.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpPathLoss = queueUpPathLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpAtmoLoss = queueUpAtmoLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpRainLoss = queueUpRainLoss.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpLinkMargin = queueUpLinkMargin.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpTime = queueUpTime.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpASM = queueUpASM.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpFrame = queueUpFrame.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataUpSNR = queueUpSNR.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty

        try:
            dataConfig = queueConfig.get(block=False)
        except queue.Empty:
            pass
            # Do nothing because Queue is empty
        try:
            dataTimeDiff = queueTimeDiff.get(block=False)
            if (len(dataConfig) > 0):
                transferData += (float(dataTimeDiff.total_seconds()) * dataConfig[0]/2 * dataConfig[1]) - ( (float(dataTimeDiff.total_seconds()) * dataConfig[0]/2 * dataConfig[1]) / dataConfig[2])
        except queue.Empty:
            pass
            # Do nothing because Queue is empty

        dataWrite = [datetime.now().time(), dataBPSKTime, dataBPSKSlantRange, dataBPSKElevation, dataBPSKPathLoss, dataBPSKLinkMargin,
                     dataBPSKRainLoss, dataBPSKAtmoLoss, dataBPSKASM, dataBPSKFrame, dataBPSKSNR, dataQPSKTime,
                     dataQPSKSlantRange, dataQPSKElevation, dataQPSKPathLoss, dataQPSKLinkMargin,
                     dataQPSKRainLoss, dataQPSKAtmoLoss, dataQPSKASM, dataQPSKFrame, dataQPSKSNR, dataUpTime,
                     dataUpSlantRange, dataUpElevation, dataUpPathLoss, dataUpLinkMargin,
                     dataUpRainLoss, dataUpAtmoLoss, dataUpASM, dataUpFrame, dataUpSNR, dataConfig, transferData]

        dataCheck = [dataBPSKTime, dataBPSKSlantRange, dataBPSKElevation, dataBPSKPathLoss, dataBPSKLinkMargin,
                     dataBPSKRainLoss, dataBPSKAtmoLoss, dataBPSKASM, dataBPSKFrame, dataBPSKSNR, dataQPSKTime,
                     dataQPSKSlantRange, dataQPSKElevation, dataQPSKPathLoss, dataQPSKLinkMargin,
                     dataQPSKRainLoss, dataQPSKAtmoLoss, dataQPSKASM, dataQPSKFrame, dataQPSKSNR, dataUpTime,
                     dataUpSlantRange, dataUpElevation, dataUpPathLoss, dataUpLinkMargin,
                     dataUpRainLoss, dataUpAtmoLoss, dataUpASM, dataUpFrame, dataUpSNR]

        dataConfig = [2, 128000, 2]

        if (dataUpTime != "") & (dataOld != dataCheck):
            filewriter.writerow(dataWrite)
            dataOld = dataCheck
            tempPrint = ""
            for e in dataWrite:
                tempPrint += str(e)
                tempPrint += ", "
            print("\r" + tempPrint, end="")
        file.flush()
