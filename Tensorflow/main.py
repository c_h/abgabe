from operator import itemgetter

import tensorflow as tf
import numpy as np
import random
import math
from multiprocessing.dummy import Process, Queue
import json
from socket import *
import struct
from time import sleep

def addToBuffer(mod, cod, sym, fThrp, fBW, fSpec_eff, fPwr_eff, fPwr_con, SNRin, score):
    global buffer, time, fitnessWeights, dtype
    buffer = np.vstack((buffer, [time, mod, cod, sym, float(fitnessWeights[0] * fThrp), float(fitnessWeights[1] * fBW),
                                 float(fitnessWeights[2] * fSpec_eff),
                                 float(fitnessWeights[3] * fPwr_eff), float(fitnessWeights[4] * fPwr_con), float(SNRin),
                                 float(score)]))
    time += 1
    buffer = buffer[buffer[:, 0].argsort()]
    return True


def dumpBuffer(pruneFrac):
    global buffer
    dumpNumb = int((1 - pruneFrac) * len(buffer))
    for j in range(0, dumpNumb):
        buffer = np.delete(buffer, (0), axis=0)
    return True


def createTrainSets():
    global buffer
    exploreTrainIn = []
    exploreTrainOut = []
    exploitTrainIn = []
    exploitTrainOutMod = []
    exploitTrainOutCod = []
    exploitTrainOutSym = []
    exploreTestIn =[]
    exploreTestOut = []
    explorValIn = []
    explorValOut = []
    exploitTestIn = []
    exploitTestOut = []
    exploitValIn = []
    epxloitValOut = []
    count = len(buffer)
    for ij in range(0, count):
        exploreTrainIn.append([buffer[ij][1], buffer[ij][2], buffer[ij][3], buffer[ij][9]])
        exploreTrainOut.append([buffer[ij][10]])
        exploitTrainIn.append([buffer[ij][4], buffer[ij][5], buffer[ij][6], buffer[ij][7], buffer[ij][8],
                               buffer[ij][9]])
        exploitTrainOutMod.append([buffer[ij][1]])
        exploitTrainOutCod.append([buffer[ij][2]])
        exploitTrainOutSym.append([buffer[ij][3]])
    exploreTrainIn, exploreTestIn, explorValIn = np.split(exploreTrainIn, [int(0.7 * len(exploreTrainIn)), int(0.85 * len(exploreTrainIn))])
    exploreTrainOut, exploreTestOut, explorValOut = np.split(exploreTrainOut, [int(0.7 * len(exploreTrainOut)),
                                                                           int(0.85 * len(exploreTrainOut))])
    exploitTrainIn, exploitTestIn, exploitValIn = np.split(exploitTrainIn, [int(0.7 * len(exploitTrainIn)),
                                                                           int(0.85 * len(exploitTrainIn))])
    exploitTrainOutMod, exploitTestOutMod, epxloitValOutMod = np.split(exploitTrainOutMod, [int(0.7 * len(exploitTrainOutMod)),
                                                                              int(0.85 * len(exploitTrainOutMod))])
    exploitTrainOutCod, exploitTestOutCod, epxloitValOutCod = np.split(exploitTrainOutCod, [int(0.7 * len(exploitTrainOutCod)),
                                                                                int(0.85 * len(exploitTrainOutCod))])
    exploitTrainOutSym, exploitTestOutSym, epxloitValOutSym = np.split(exploitTrainOutSym, [int(0.7 * len(exploitTrainOutSym)),
                                                                                int(0.85 * len(exploitTrainOutSym))])
    return exploreTrainIn, exploreTrainOut, exploitTrainIn, exploitTrainOutMod, exploitTrainOutCod, exploitTrainOutSym, exploreTestIn, exploreTestOut, explorValIn, explorValOut, exploitTestIn, exploitValIn, exploitTestOutMod, epxloitValOutMod, exploitTestOutCod, epxloitValOutCod, exploitTestOutSym, epxloitValOutSym


def getRollBackThres():
    global buffer
    tempThres = 0.0
    for f in range(0, len(buffer)):
        if buffer[f][10] > tempThres:
            tempThres = buffer[f][10]
    return tempThres * 0.9


def predictExlpore(SNR):
    global actions, actionList, explModel, explCounter
    results = []
    tempResults = []
    for elem in explModel:
        for i in range(actions):
            input = [actionList[i][1], actionList[i][3], actionList[i][4], SNR]
            tempResults.append([i, elem.predict([input])])
        results.append(tempResults)
    explCounter += 1
    return results


# Predict Action with Exploit Network
def predictExploit():
    global exploitMatrix, exploitInput, actionList, actionListNotNormal
    result = []
    for elem in exploitMatrix:
        result.append([elem[0].predict([exploitInput]), elem[1].predict([exploitInput]),
              elem[2].predict([exploitInput])])
    tempMod = 0
    tempCod = 0
    tempSym = 0
    for i in range(len(exploitMatrix)):
        tempMod += result[i][0]
        tempCod += result[i][1]
        tempSym += result[i][2]
    tempMod = tempMod/len(exploitMatrix)
    tempCod = tempCod/len(exploitMatrix)
    tempSym = tempSym/len(exploitMatrix)
    tempMod = min([2,4], key=lambda x: abs(x - denormalizeData(tempMod,0,4)))
    tempCod = min([2,3,4,6,8], key=lambda x: abs(x - denormalizeData(tempCod,0,8)))
    tempSym = min([*range(50000, 1500000, 50000)], key=lambda x: abs(x - denormalizeData(tempSym,0,1500000)))
    for i in range(actions):
        if actionListNotNormal[i][1] == tempMod and actionListNotNormal[i][3] == tempCod and actionListNotNormal[i][
            4] == tempSym:
            return i


def trainNN():
    global trainingComplete, exploreTrainIn, exploreTrainOut, exploitTrainIn, exploitTrainOutMod, exploitTrainOutCod, exploitTrainOutSym, explMatrix, exploitMatrix, exploitNN, trainCounter, exploreTestIn, exploreTestOut, explorValIn, explorValOut, exploitTestIn, exploitValIn, exploitTestOutMod, epxloitValOutMod, exploitTestOutCod, epxloitValOutCod, exploitTestOutSym, epxloitValOutSym
    for elem in explMatrix:
        elem.fit(exploreTrainIn, exploreTrainOut, epochs=1, verbose=0, batch_size=1, validation_data=(explorValIn,explorValOut))
        elem.evaluate(exploreTestIn, exploreTestOut, verbose=0)
    for elem in exploitMatrix:
        elem[0].fit(exploitTrainIn, exploitTrainOutMod, epochs=1, verbose=0, batch_size=1, validation_data=(exploitValIn,epxloitValOutMod))
        elem[0].evaluate(exploitTestIn, exploitTestOutMod, verbose=0)
        elem[1].fit(exploitTrainIn, exploitTrainOutCod, epochs=1, verbose=0, batch_size=1, validation_data=(exploitValIn,epxloitValOutCod))
        elem[1].evaluate(exploitTestIn, exploitTestOutCod, verbose=0)
        elem[2].fit(exploitTrainIn, exploitTrainOutSym, epochs=1, verbose=0, batch_size=1, validation_data=(exploitValIn,epxloitValOutSym))
        elem[2].evaluate(exploitTestIn, exploitTestOutSym, verbose=0)
    trainingComplete = True
    trainCounter += 1

# Logic of cognitiv engine
def findNextAction(SNR):
    global epsilon, epsilonIteration, nnTrained, forceExplore, exploreNNMaxPerfThresh, firstExploreAfterNNTrained, actions, nnRejectionRate, exploitFlag, actionID
    actionID = -1
    if epsilon > epsilonResetLimit:
        epsilon = 1.0 / float(epsilonIteration)
        epsilonIteration += 1
    else:  # EpsilonReset
        epsilon = 1.0
        epsilonIteration = 1
    if (nnTrained and not forceExplore):
        prob = random.random()
        if prob <= epsilon:  # Exploration Mode
            exploreScores = predictExlpore(SNR)
            maxScore = 0.0
            tempAction = 0
            for b in range(0, len(exploreScores[0])):
                tempScore = 0.0
                for elem in exploreScores:
                    tempScore += elem[b][1]
                tempScore = tempScore / float(len(exploreScores))
                if tempScore > maxScore:
                    maxScore = tempScore
                    tempAction = exploreScores[0][b][0]
            perfThresh = maxScore * exploreNNMaxPerfThresh
            if firstExploreAfterNNTrained:
                actionID = tempAction
                firstExploreAfterNNTrained = False
            else:
                goodAction = []
                badAction = []
                for c in range(0, len(exploreScores[0])):
                    tempScore = 0.0
                    for elem in exploreScores:
                        tempScore += elem[c][1]
                    tempScore = tempScore / len(exploreScores)
                    if tempScore < perfThresh:
                        badAction.append(exploreScores[0][c])
                    else:
                        goodAction.append(exploreScores[0][c])
                if len(goodAction) == 0 or len(badAction) == 0:
                    actionID = actions
                    while actionID >= actions:
                        actionID = int(math.floor(float(actions) * random.random()))
                else:
                    if random.random() >= nnRejectionRate:  # Bad Action
                        id = len(badAction)
                        while (id >= len(badAction)):
                            id = int(math.floor(float(len(badAction) * random.random())))
                        actionID = badAction[id][0]
                    else:
                        id = len(goodAction)
                        while (id >= len(goodAction)):
                            id = int(math.floor(float(len(goodAction) * random.random())))
                        actionID = goodAction[id][0]
            exploitFlag = False
        else:  # Exploit Mode
            actionID = predictExploit()
            exploitFlag = True
    else:  # If reset of if first history buffer
        if currentlyTraining and nnTrained:  # If in Training Exploit
            actionID = predictExploit()
            exploitFlag = True
        else:  # Random Explore
            actionID = actions
            while (actionID >= actions):
                actionID = int(math.floor(float(actions) * random.random()))
            exploitFlag = False
    return actionID


def equalExploitInput(exploitInput):
    fl = True
    for x in range(0, 6):
        if abs(exploitInput[x] - lastExploitInput[x]) > math.exp(-12):
            fl = False
    return fl


# Calculate Values and define Exploit Input and contol Training of NN with response, as in RLNN2 Paper
def recordResponse(SNRin, actionID, penalty):
    global currentlyTraining, forceExplore, buffer, lastExploitFitObserved, fitObservedMax, exploitFlag, exploitInput, forceExploreThreshold, lastExploitInput, histRollBackCnt, histRollBackIdx, nnTrained, firstExploreAfterNNTrained, exploitModel, explModel, trainingComplete
    fThrp = actionList[actionID][4] * actionList[actionID][2] * actionList[actionID][3]
    fBW = actionList[actionID][4] * (1 + 0.35)  # Static Rolloff 0.35
    fSpecEff = actionList[actionID][2] * actionList[actionID][3] / (1 + 0.35)
    fPwr_eff = (actionList[actionID][2] * actionList[actionID][3]) / (
                actionList[actionID][4] * pow(10, SNRin / 10))
    fPwr_con = actionList[actionID][4] * 0  # No addtional Power
    fitObserved = (fitnessWeights[0] * fThrp + fitnessWeights[1] * fBW + fitnessWeights[2] * fSpecEff + fitnessWeights[
        3] * fPwr_eff + fitnessWeights[4] * fPwr_con) * penalty
    if forceExplore:
        if len(buffer) == 0:
            lastExploitFitObserved = fitObserved
        else:
            if fitObserved > lastExploitFitObserved:
                lastExploitFitObserved = fitObserved
    if fitObserved >= fitObservedMax:
        fitObservedMax = fitObserved
        if not exploitFlag:
            exploitInput = [fitnessWeights[0] * fThrp, fitnessWeights[1] * fBW, fitnessWeights[2] * fSpecEff,
                            fitnessWeights[3] * fPwr_eff, fitnessWeights[4] * fPwr_con, SNRin]
    else:
        if exploitFlag:
            if fitObserved < lastExploitFitObserved:
                if (((lastExploitFitObserved - fitObserved) > forceExploreThreshold) and (
                        equalExploitInput(exploitInput)) and ((lastExploitInput != [0, 0, 0, 0, 0, 0]).all())) or ((
                        (histRollBackCnt >= len(buffer)) and (equalExploitInput(exploitInput)) and (
                        (lastExploitInput != [0, 0, 0, 0, 0, 0]).all()))) and not currentlyTraining:
                    forceExplore = True
                    fitObservedMax = 0
                    buffer = np.array([])
                    histRollBackCnt = 0
                elif fitObserved < lastExploitFitObserved * 0.9 or fitObserved < getRollBackThres():
                    histRollBackCnt = histRollBackCnt + 1
                    histRollBackIdx = histRollBackIdx + 1
                    if histRollBackIdx >= len(buffer) - 1:
                        histRollBackIdx = 0
                    tempBuffer = buffer[buffer[:, 10].argsort()]
                    exploitInput = [tempBuffer[len(buffer) - 1 - histRollBackIdx][4],
                                    tempBuffer[len(buffer) - 1 - histRollBackIdx][5],
                                    tempBuffer[len(buffer) - 1 - histRollBackIdx][6],
                                    tempBuffer[len(buffer) - 1 - histRollBackIdx][7],
                                    tempBuffer[len(buffer) - 1 - histRollBackIdx][8],
                                    tempBuffer[len(buffer) - 1 - histRollBackIdx][9]]
                elif fitObserved > lastExploitFitObserved * 0.9 and histRollBackIdx > -1:
                    lastExploitFitObserved = fitObserved
                    lastExploitInput = exploitInput
                    histRollBackCnt = 0
                else:
                    if lastExploitInput != [0,0,0,0,0,0]:
                        exploitInput = lastExploitInput
                        histRollBackCnt = 0
            else:
                lastExploitFitObserved = fitObserved
                lastExploitInput = exploitInput
                histRollBackCnt = 0
    addToBuffer(actionList[actionID][1], actionList[actionID][3], actionList[actionID][4], fThrp, fBW, fSpecEff,
                fPwr_eff, fPwr_con, SNRin, fitObserved)
    if (len(buffer) >= buf_nTrainTestSamples):
        global explMatrix, exploitNN, queueExploreNN, queueExploitNN, processTrain, exploreTrainSet, exploitTrainSet, explModel, exploitModel, exploreTrainIn, exploreTrainOut, exploitTrainIn, exploitTrainOutMod, exploitTrainOutCod, exploitTrainOutSym, exploreTestIn, exploreTestOut, explorValIn, explorValOut, exploitTestIn, exploitValIn, exploitTestOutMod, epxloitValOutMod, exploitTestOutCod, epxloitValOutCod, exploitTestOutSym, epxloitValOutSym
        if not currentlyTraining:
            currentlyTraining = True
            trainingComplete = False
            exploreTrainIn, exploreTrainOut, exploitTrainIn, exploitTrainOutMod, exploitTrainOutCod, exploitTrainOutSym, exploreTestIn, exploreTestOut, explorValIn, explorValOut, exploitTestIn, exploitValIn, exploitTestOutMod, epxloitValOutMod, exploitTestOutCod, epxloitValOutCod, exploitTestOutSym, epxloitValOutSym = createTrainSets()
            processTrain = Process(target=trainNN)
            processTrain.start()
        if trainingComplete:
            processTrain.join()
            explModel = explMatrix
            exploitModel = exploitMatrix
            currentlyTraining = False
            nnTrained = True
            forceExplore = False
            firstExploreAfterNNTrained = True
            dumpBuffer(pruneFrac)
    return fitObserved

#Use if Exploitation only gets Measurmentinputs
def recordResponseSimpel(SNRin, actionID, penalty):
    global currentlyTraining, forceExplore, buffer, lastExploitFitObserved, fitObservedMax, exploitFlag, exploitInput, forceExploreThreshold, lastExploitInput, histRollBackCnt, histRollBackIdx, nnTrained, firstExploreAfterNNTrained, exploitModel, explModel, trainingComplete
    fThrp = actionList[actionID][4] * actionList[actionID][2] * actionList[actionID][3]
    fBW = actionList[actionID][4] * (1 + 0.35)  # Static Rolloff 0.35
    fSpecEff = actionList[actionID][2] * actionList[actionID][3] / (1 + 0.35)
    fPwr_eff = (actionList[actionID][2] * actionList[actionID][3]) / (
            actionList[actionID][4] * pow(10, SNRin / 10))
    fPwr_con = actionList[actionID][4] * 0  # No addtional Power
    fitObserved = (fitnessWeights[0] * fThrp + fitnessWeights[1] * fBW + fitnessWeights[2] * fSpecEff + fitnessWeights[
        3] * fPwr_eff + fitnessWeights[4] * fPwr_con) * penalty
    exploitInput = [fitnessWeights[0] * fThrp, fitnessWeights[1] * fBW, fitnessWeights[2] * fSpecEff,
                            fitnessWeights[3] * fPwr_eff, fitnessWeights[4] * fPwr_con, SNRin]
    addToBuffer(actionList[actionID][1], actionList[actionID][3], actionList[actionID][4], fThrp, fBW, fSpecEff,
                fPwr_eff, fPwr_con, SNRin, fitObserved)
    if (len(buffer) >= buf_nTrainTestSamples):
        global explMatrix, exploitNN, queueExploreNN, queueExploitNN, processTrain, exploreTrainSet, exploitTrainSet, explModel, exploitModel, exploreTrainIn, exploreTrainOut, exploitTrainIn, exploitTrainOutMod, exploitTrainOutCod, exploitTrainOutSym, exploreTestIn, exploreTestOut, explorValIn, explorValOut, exploitTestIn, exploitValIn, exploitTestOutMod, epxloitValOutMod, exploitTestOutCod, epxloitValOutCod, exploitTestOutSym, epxloitValOutSym
        if not currentlyTraining:
            currentlyTraining = True
            trainingComplete = False
            exploreTrainIn, exploreTrainOut, exploitTrainIn, exploitTrainOutMod, exploitTrainOutCod, exploitTrainOutSym, exploreTestIn, exploreTestOut, explorValIn, explorValOut, exploitTestIn, exploitValIn, exploitTestOutMod, epxloitValOutMod, exploitTestOutCod, epxloitValOutCod, exploitTestOutSym, epxloitValOutSym = createTrainSets()
            processTrain = Process(target=trainNN)
            processTrain.start()
        if trainingComplete:
            processTrain.join()
            explModel = explMatrix
            exploitModel = exploitMatrix
            currentlyTraining = False
            nnTrained = True
            forceExplore = False
            firstExploreAfterNNTrained = True
            dumpBuffer(pruneFrac)
    return fitObserved

def normalizeData(data, min, max):
    return (data - min) / (max - min)


def denormalizeData(data, min, max):
    return (data * (max - min)) + min


if __name__ == '__main__':
    global exploreNN, queueExploreNN, queueExploitNN, processTrain, exploreTrainSet, exploitTrainSet, exploreTrainIn, exploreTrainOut, exploitTrainIn, exploitTrainOut, exploitModel, explModel, actionListNotNormal, dtype, exploitMatrix, explMatrix, trainCounter, explCounter, buf_nTrainTestSamples
    global epsilonResetLimit, exploreNNMaxPerfThresh, nnRejectionRate, trainFrac, pruneFrac, forceExploreThreshold, epsilon, epsilonIteration, nnTrained, firstExploreAfterNNTrained, buf_nTrainTestSamples, actions, exploitFlag, fitnessWeights, fitObservedMax, lastExploitFitObserved, forceExplore, histRollBackIdx, histRollBackCnt, currentlyTraining, trainingComplete, exploitInput, lastExploitInput, buffer
    exploitModel = ""
    explModel = ""
    exploreTrainIn = []
    exploreTrainOut = []
    exploitTrainIn = []
    exploitTrainOut = []
    processTrain = Process(target=trainNN)
    trainCounter = 0

    explMatrix = []
    for i in range(0, 20):
        exploreNN = tf.keras.models.Sequential()
        exploreNN.add(tf.keras.layers.Dense(4, activation='sigmoid'))
        exploreNN.add(tf.keras.layers.Dense(7, activation='sigmoid'))
        exploreNN.add(tf.keras.layers.Dropout(0.1))
        exploreNN.add(tf.keras.layers.Dense(50, activation='sigmoid'))
        exploreNN.add(tf.keras.layers.Dropout(0.1))
        exploreNN.add(tf.keras.layers.Dense(1, activation='linear'))

        exploreNN.compile(optimizer='adam', loss=tf.keras.losses.MeanSquaredError(), metrics=tf.keras.metrics.MeanSquaredError())
        explMatrix.append(exploreNN)
    explModel = explMatrix

    global exploitNN
    exploitNN = []
    exploitMatrix = []
    for j in range(0, 10):
        for i in range(0, 3):
            tempModel = tf.keras.models.Sequential([
                tf.keras.layers.Dense(6, activation='sigmoid'),
                tf.keras.layers.Dense(20, activation='sigmoid'),
                tf.keras.layers.Dropout(0.1),
                tf.keras.layers.Dense(1, activation='linear')])
            tempModel.compile(optimizer='adam', loss=tf.keras.losses.MeanSquaredError(), metrics=tf.keras.metrics.MeanSquaredError())
            exploitNN.append(tempModel)
        exploitMatrix.append(exploitNN)
    exploitModel = exploitMatrix

    # Data Object for all possible Configurations
    global actionList
    actionList = []
    actionListNotNormal = []
    tempId = 0
    modList = [2, 4]
    symList = [1, 2]
    codList = [2, 3, 4, 6, 8]
    symRate = [*range(50000, 1550000, 50000)]
    for i0 in range(0, len(modList)):
        for i1 in range(0, len(codList)):
            for i2 in range(0, len(symRate)):
                actionList.append([tempId, normalizeData(modList[i0],0,4), normalizeData(symList[i0],0,2), normalizeData(codList[i1],0,8), normalizeData(symRate[i2],0,1500000)])
                actionListNotNormal.append([tempId, modList[i0], symList[i0], codList[i1], symRate[i2]])
                tempId += 1


    # RLNN Control Values
    epsilonResetLimit = 0.0001
    exploreNNMaxPerfThresh = 0.9
    nnRejectionRate = 0.75
    trainFrac = 0.9
    pruneFrac = 0.5  # % to keep in buffer after dumping
    forceExploreThreshold = 0.95
    epsilon = 1.0  # Value Decide Explore/Exploit
    epsilonIteration = 1  # Value for epsilon change function
    nnTrained = False
    firstExploreAfterNNTrained = False
    buf_nTrainTestSamples = 400  # Size of buffer
    actions = len(actionList)
    exploitFlag = False
    fitnessWeights = [0.2, 0.2, 0.2, 0.2, 0.2]  # Weights for different Scenarios and Priority
    fitObservedMax = 0.0
    lastExploitFitObserved = 0.0
    forceExplore = True
    histRollBackIdx = -1
    histRollBackCnt = 0
    currentlyTraining = False
    trainingComplete = False
    exploitInput = []
    lastExploitInput = [0, 0, 0, 0, 0, 0]

    # Trainings Data Buffer
    buffer = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    time = 0
    UDPSock = socket(AF_INET, SOCK_DGRAM)
    UDPSock.bind(("127.0.0.1", 6664))
    UDPSockP = socket(AF_INET, SOCK_DGRAM)
    UDPSockP.bind(("127.0.0.1", 6667))
    UDPSockP.settimeout(0.01)
    SNRRaw = 1
    tempFit = 0
    explCounter = 0
    while (True):
        tempID = findNextAction(normalizeData(SNRRaw,0,30))
        tempJson = json.dumps({"mod":actionListNotNormal[tempID][1], "coderate":actionListNotNormal[tempID][3], "symrate":actionListNotNormal[tempID][4], "exploit":exploitFlag, "fit":tempFit, "train":trainCounter, "time":time, "expl":explCounter}).encode('utf8')
        UDPSock.sendto(tempJson, ("127.0.0.1", 6665))
        sleep(0.01)
        data, addr = UDPSock.recvfrom(1024)
        temp = [struct.unpack('!f', data)[0]]
        SNRRaw = float(temp[0])
        penalty = 1
        try:
            pen, addr = UDPSockP.recvfrom(1024)
            penalty = 0
        except:
            pass
        tempSNR = normalizeData(SNRRaw,0,30)
        tempFit = recordResponseSimpel(tempSNR, tempID, penalty)
        #tempFit = recordResponse(tempSNR, tempID, penalty)
